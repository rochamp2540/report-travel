const express = require("express");
const router = express();
const Province = require("./../models/province_schema");
const Transport = require("./../models/transport_schema");
const FormTravelling = require("../models/travelling_schema");

router.get("/province", async (req, res) => {
  try {
    const province = await Province.find({});
    // console.log(province);
    res.json(province);
  } catch (e) {
    res.json(e);
  }
});

router.get("/transport", async (req, res) => {
  try {
    const transport = await Transport.find({});
    // console.log(transport);
    res.json(transport);
  } catch (e) {
    console.log(e);
  }
});

router.post("/formSubmit", async (req, res) => {
  try {
    // req.body.created_by = localStorage.getItem();
    let doc = await FormTravelling.create(req.body);
    if (doc) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e);
  }
});

router.put("/formUpdate", async (req, res) => {
  try {
    let doc = await FormTravelling.findOneAndUpdate(
      {
        travelling_id: req.body.travelling_id,
      },
      req.body
    );
    if (doc) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e);
  }
});

router.get("/formSubmit/id/:id", async (req, res) => {
  let doc = await FormTravelling.findOne({ travelling_id: req.params.id });
  res.json(doc);
});
module.exports = router;
