const express = require("express");

const router = express.Router();
const Users = require("./../models/user_schema");
const bcrypt = require("bcryptjs");
// const jwt = require("./jwt");
// const randtoken = require("rand-token");
// const refreshTokens = {};

router.get("/login", (req, res) => {
  res.end("login");
});

router.post("/login", async (req, res) => {
  try {
    // console.log(req.body);
    const doc = await Users.findOne({ username: req.body.username });
    // console.log(doc)
    if (doc) {
      const isValidPassword = await bcrypt.compare(
        req.body.password,
        doc.password
      );
      if (isValidPassword) {
        // const payload = {
        //   id: doc2._id,
        //   level: doc2.level,
        //   username: doc2.username,
        // };
        // const token = jwt.sign(payload, "100000");
        const token = doc.username;
        // const refreshToken = randtoken.uid(256);
        // refreshTokens[refreshToken] = payload.username;
        // res.json({ result: "ok", token, refreshToken, message: "success" });
        res.json({ result: "ok", token, message: "success" });
      } else {
        res.json({ result: "nok", message: "Password is wrong" });
      }
    } else {
      res.json({ result: "nokk", message: "username not found" });
    }
  } catch (e) {
    res.json(e);
  }
});

router.post("/register", async (req, res) => {
  try {
    // console.log(req.body)
    req.body.password = await bcrypt.hash(req.body.password, 8);
    const doc = await Users.create(req.body);
    if (doc) {
      res.json({ result: "ok" });
    } else {
      res.json({ result: "nok" });
    }
  } catch (e) {
    res.json({ result: "nokk", message: "internal error" });
    console.log(e); //อยากรู้เฉยๆ
  }
});

// // Refresh Token
// let count = 1;
// router.post("/refresh/token", function (req, res) {
//   const refreshToken = req.body.refreshToken;
//   console.log("Refresh Token : " + count++);
//   console.log(refreshToken)
//   console.log(refreshTokens)
//   if (refreshToken in refreshTokens) {
//     const payload = {
//       username: refreshTokens[refreshToken],
//       level: "normal",
//     };
//     const token = jwt.sign(payload, "100000"); // unit is millisec
//     res.json({ jwt: token });
//   } else {
//     console.log("Not found");
//     return res
//       .status(403)
//       .json({ auth: false, message: "Invalid refresh token" });
//   }
// });

module.exports = router;
