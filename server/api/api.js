const express = require("express");
const router = express.Router();

require("./../database/db");
router.use(require("./api_auth"));
router.use(require("./api_form"));
router.use(require("./api_list"));

module.exports = router;
