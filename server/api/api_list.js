const express = require("express");
const router = express();
const FormTravelling = require("../models/travelling_schema");

router.get("/list", async (req, res) => {
  try {
    var token = req.headers["x-access-token"];
    var list = null;
    console.log("token: " + token);
    token === "admin1"
      ? (list = await FormTravelling.aggregate([
          {
            $lookup: {
              from: "provinces",  //ตาราง provinces
              localField: "province_id", // province_id ของ travelling
              foreignField: "ProvinceID", // ProvinceID ของ provinces
              as: "proname", // เอา obj ของ provinces ที่ province_id == ProvinceID มาเพิ่ม key ที่ตาราง travelling ด้วยชื่อ proname
            },
          },
          { $unwind: "$proname" }, // แตก array เป็น obj ex. {a:"A", b:[1,2]} -> {a:"A", b:1}, {a:"A", b:2} 
          {
            $addFields: {
              province_id: "$proname.ProvinceEng", //เปลี่ยนหรือเพิ่ม? ตาราง travelling แทนที่ด้วย ProvinceEng
            },
          },
        ]))
      : (list = await FormTravelling.aggregate([
          { $match: { created_by: token } }, //ตาราง travelling คนไหนสร้าง
          {
            $lookup: {
              from: "provinces", //ตาราง provinces
              localField: "province_id", // province_id ของ travelling
              foreignField: "ProvinceID", // ProvinceID ของ provinces
              as: "proname",
            },
          },
          { $unwind: "$proname" }, // เปลี่ยนชื่อ
          {
            $addFields: {
              province_id: "$proname.ProvinceEng", //เปลี่ยนหรือเพิ่ม? ตาราง travelling แทนที่ด้วย ProvinceEng
            },
          },
        ]));
    res.json(list);
  } catch (e) {
    console.log(e);
  }
});
module.exports = router;
