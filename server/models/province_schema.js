const mongoose = require("mongoose");

const provincesSchema = mongoose.Schema({
  // _id: { type: mongoose.Schema.Types.ObjectId, auto: true },
  provinceID: Number,
  provinceEng: String,
  // provinceId: {
  //   type: String,
  // },
  // provinceEng: {
  //   type: String,
  // },
});
module.exports = mongoose.model("provinces", provincesSchema);
