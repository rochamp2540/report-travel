const mongoose = require("mongoose");

const transportSchema = mongoose.Schema({
  transport_id: Number,
  transport: String,
});

module.exports = mongoose.model("transports", transportSchema);
