const mongoose = require("mongoose");
const AutoIncrement = require("mongoose-sequence")(mongoose);

const travellingSchema = mongoose.Schema(
  {
    _id: { type: mongoose.Schema.Types.ObjectId, auto: true },
    province_id: Number, //join
    date_start: Date,
    date_end: Date,
    information: String,
    habitation: String,
    transport_id: Number, //join
    acknowledge: { type: Boolean, default: false },
    acknowledge_by: { type: String, default: "" }, //join
    acknowledge_date: { type: Date, default: "" },
    created_by: String, //join
    created_date: { type: Date, default: Date.now },
    // user_id: { type: mongoose.Schema.Types.ObjectId, required: true },
  },
  { _id: false }
);

travellingSchema.plugin(AutoIncrement, { inc_field: "travelling_id" });
module.exports = mongoose.model("travelling", travellingSchema);
