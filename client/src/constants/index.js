// Login Page
export const LOGIN_FETCHING = "LOGIN_FETCHING";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGOUT = "LOGOUT";

// Register Page
export const REGISTER_FETCHING = "REGISTER_FETCHING";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILED = "REGISTER_FAILED";

// Form Page province
export const FORM_PROVINCE_FETCHING = "FORM_PROVINCE_FETCHING";
export const FORM_PROVINCE_SUCCESS = "FORM_PROVINCE_SUCCESS";
export const FORM_PROVINCE_FAILED = "FORM_PROVINCE_FAILED";
// Form Page transport
export const FORM_TRANSPORT_FETCHING = "FORM_TRANSPORT_FETCHING";
export const FORM_TRANSPORT_SUCCESS = "FORM_TRANSPORT_SUCCESS";
export const FORM_TRANSPORT_FAILED = "FORM_TRANSPORT_FAILED";
// Form Page submit
export const FORM_SUBMIT_FETCHING = "FORM_SUBMIT_FETCHING";
export const FORM_SUBMIT_SUCCESS = "FORM_SUBMIT_SUCCESS";
export const FORM_SUBMIT_FAILED = "FORM_SUBMIT_FAILED";
//List Page
export const LIST_FETCHING = "LIST_FETCHING";
export const LIST_SUCCESS = "LIST_SUCCESS";
export const LIST_FAILED = "LIST_FAILED";

// Error Code
export const E_PICKER_CANCELLED = "E_PICKER_CANCELLED";
export const E_PICKER_CANNOT_RUN_CAMERA_ON_SIMULATOR =
  "E_PICKER_CANNOT_RUN_CAMERA_ON_SIMULATOR";
export const E_PERMISSION_MISSING = "E_PERMISSION_MISSING";
export const E_PICKER_NO_CAMERA_PERMISSION = "E_PICKER_NO_CAMERA_PERMISSION";
export const E_USER_CANCELLED = "E_USER_CANCELLED";
export const E_UNKNOWN = "E_UNKNOWN";
export const E_DEVELOPER_ERROR = "E_DEVELOPER_ERROR";
export const TIMEOUT_NETWORK = "ECONNABORTED"; // request service timeout
export const NOT_CONNECT_NETWORK = "NOT_CONNECT_NETWORK";

//////////////// Localization Begin ////////////////
export const NETWORK_CONNECTION_MESSAGE =
  "Cannot connect to server, Please try again.";
export const NETWORK_TIMEOUT_MESSAGE =
  "A network timeout has occurred, Please try again.";
export const UPLOAD_PHOTO_FAIL_MESSAGE =
  "An error has occurred. The photo was unable to upload.";

export const apiUrl = "http://localhost:8080/api/";
export const imageUrl = "http://localhost:8080";

export const server = {
  LOGIN_URL: `login`,
  REFRESH_TOKEN_URL: `refresh/token`,
  REGISTER_URL: `register`,
  TOKEN_KEY: `token`,
  REFRESH_TOKEN_KEY: `refresh_token`,
  PROVINCE_URL: `province`,
  TRANSPORT_URL: `transport`,
  FORMSUBMIT_URL: `formSubmit`,
  FORMUPDATE_URL: `formUpdate`,
  TRAVELLINGLIST_URL: `list`,
};
