import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import reducers from "./reducers";
import logger from "redux-logger";
import "bootstrap/dist/css/bootstrap.css";

var dev_middlewares = [];
if (process.env.NODE_ENV === `development`) {
  dev_middlewares.push(logger);
}
const store = createStore(reducers, applyMiddleware(thunk, ...dev_middlewares)); //reducers, middlewares
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
