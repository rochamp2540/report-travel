import {
  FORM_PROVINCE_FETCHING,
  FORM_PROVINCE_SUCCESS,
  FORM_PROVINCE_FAILED,
  FORM_TRANSPORT_FETCHING,
  FORM_TRANSPORT_SUCCESS,
  FORM_TRANSPORT_FAILED,
  FORM_SUBMIT_FETCHING,
  FORM_SUBMIT_SUCCESS,
  FORM_SUBMIT_FAILED,
} from "./../constants/index";

const initialState = {
  result: [], //province
  result2: [], //transport
  result3: [], //submit
  isFetching: false,
  isError: false,
};

//province
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FORM_PROVINCE_FETCHING:
      return { ...state, result: null, isFetching: true, isError: false };
    case FORM_PROVINCE_SUCCESS:
      return {
        ...state,
        result: payload,
        isFetching: false,
        isError: false,
      };
    case FORM_PROVINCE_FAILED:
      return { ...state, result: null, isFetching: false, isError: true };
    // --------------------------------------------------------------------------------
    //transport
    case FORM_TRANSPORT_FETCHING:
      return { ...state, result2: null, isFetching: true, isError: false };
    case FORM_TRANSPORT_SUCCESS:
      return {
        ...state,
        result2: payload,
        isFetching: false,
        isError: false,
      };
    case FORM_TRANSPORT_FAILED:
      return { ...state, result2: null, isFetching: false, isError: true };
    // --------------------------------------------------------------------------------
    //submit
    case FORM_SUBMIT_FETCHING:
      return { ...state, result3: null, isFetching: true, isError: false };
    case FORM_SUBMIT_SUCCESS:
      return {
        ...state,
        result3: payload,
        isFetching: false,
        isError: false,
      };
    case FORM_SUBMIT_FAILED:
      return { ...state, result3: null, isFetching: false, isError: true };
    default:
      return state;
  }
};
