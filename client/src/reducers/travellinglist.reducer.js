import { LIST_FETCHING, LIST_SUCCESS, LIST_FAILED } from "./../constants/index";

const initialState = {
  result: [],
  isFetching: false,
  isError: false,
};
export default (state = initialState, { type, payload }) => {
  //   console.log(payload)
  switch (type) {
    case LIST_FETCHING:
      return { ...state, result: null, isFetching: true, isError: false };
    case LIST_SUCCESS:
      return { ...state, result: payload, isFetching: false, isError: false };
    case LIST_FAILED:
      return { ...state, result: null, isFetching: false, isError: true };
    default:
      return state;
  }
};
