import { combineReducers } from "redux";
import registerReducer from "./register.reducer";
import loginReducer from "./login.reducer";
import travellingFormReducer from "./travellingform.reducer";
import travellinglistReducer from "./travellinglist.reducer";

export default combineReducers({
  registerReducer,
  loginReducer,
  travellingFormReducer,
  travellinglistReducer,
});
