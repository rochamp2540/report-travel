import { httpClient } from "./../utils/HttpClient";
import {
  FORM_PROVINCE_FETCHING,
  FORM_PROVINCE_SUCCESS,
  FORM_PROVINCE_FAILED,
  FORM_TRANSPORT_FETCHING,
  FORM_TRANSPORT_SUCCESS,
  FORM_TRANSPORT_FAILED,
  FORM_SUBMIT_FETCHING,
  FORM_SUBMIT_SUCCESS,
  FORM_SUBMIT_FAILED,
  server,
} from "./../constants/index";

export const setFormProvinceToFetching = () => ({
  type: FORM_PROVINCE_FETCHING,
});

export const setFormProvinceToSuccess = (payload) => ({
  type: FORM_PROVINCE_SUCCESS,
  payload,
});

export const setFormProvinceToFailed = () => ({
  type: FORM_PROVINCE_FAILED,
});
// ------------------------------------------------------------
export const setFormTransportToFetching = () => ({
  type: FORM_TRANSPORT_FETCHING,
});

export const setFormTransportToSuccess = (payload) => ({
  type: FORM_TRANSPORT_SUCCESS,
  payload,
});

export const setFormTransportToFailed = () => ({
  type: FORM_TRANSPORT_FAILED,
});
// ------------------------------------------------------------
export const setFormSubmitToFetching = () => ({
  type: FORM_SUBMIT_FETCHING,
});

export const setFormSubmitToSuccess = (payload) => ({
  type: FORM_SUBMIT_SUCCESS,
  payload,
});

export const setFormSubmitToFailed = () => ({
  type: FORM_SUBMIT_FAILED,
});
// ------------------------------------------------------------

export const getFormById = (id) => {
  return async (dispatch) => {
    try {
      dispatch(setFormSubmitToFetching());
      let result = await httpClient.get(`${server.FORMSUBMIT_URL}/id/${id}`);
      // console.log(result)
      dispatch(setFormSubmitToSuccess(result.data));
    } catch (e) {
      alert(JSON.stringify(e));
      dispatch(setFormSubmitToFailed());
    }
  };
};

export const formUpdate = (value,history) => {
  return async (dispatch) => {
    dispatch(setFormSubmitToFetching());
    try {
      let result = await httpClient.put(server.FORMUPDATE_URL, value);
      dispatch(setFormSubmitToSuccess(result.data));
      history.push("/list")
    } catch (err) {
      // alert(JSON.stringify(err));
      dispatch(setFormSubmitToFailed());
    }
  };
};

export const formSubmit = (value,history) => {
  return async (dispatch) => {
    dispatch(setFormSubmitToFetching());
    try {
      value.created_by = localStorage.getItem(server.TOKEN_KEY);
      // console.log(value);
      let result = await httpClient.post(server.FORMSUBMIT_URL, value);
      dispatch(setFormSubmitToSuccess(result.data));
      history.push("/list")
    } catch (err) {
      // alert(JSON.stringify(err));
      dispatch(setFormSubmitToFailed());
    }
  };
};

export const getProvince = () => {
  return async (dispatch) => {
    dispatch(setFormProvinceToFetching());
    try {
      let result = await httpClient.get(server.PROVINCE_URL);
      //   console.log(result);
      dispatch(setFormProvinceToSuccess(result.data));
    } catch (err) {
      // alert(JSON.stringify(err));
      dispatch(setFormProvinceToFailed());
    }
  };
};
export const getTransport = () => {
  return async (dispatch) => {
    dispatch(setFormTransportToFetching());
    try {
      let result = await httpClient.get(server.TRANSPORT_URL);
      //   console.log(result);
      dispatch(setFormTransportToSuccess(result.data));
    } catch (err) {
      // alert(JSON.stringify(err));
      dispatch(setFormTransportToFailed());
    }
  };
};
