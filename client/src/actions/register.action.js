import {
  REGISTER_FETCHING,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
} from "./../constants/index";

import axios from "axios";

export const setRegisterStateToFetch = () => ({
  type: REGISTER_FETCHING,
});

export const setRegisterStateToSuccess = (payload) => ({
  type: REGISTER_SUCCESS,
  payload,
});

export const setRegisterStateToFailed = (payload) => ({
  type: REGISTER_FAILED,
  payload,
});

export const register = (value, history) => {
  return async (dispatch) => {
    try {
      dispatch(setRegisterStateToFetch());
      const result = await axios.post(
        "http://localhost:8080/api/register",
        value
      ); 
      if (result.data.result === "ok") {
        dispatch(setRegisterStateToSuccess(result.data));
        history.push("/login");
      } else {
        dispatch(setRegisterStateToFailed({ result: "register error" }));
        alert("register error");
      }
    } catch (e) {
      dispatch(setRegisterStateToFailed({ result: JSON.stringify(e) }));
    }
  };
};