import { httpClient } from "./../utils/HttpClient";
import {
  LIST_FETCHING,
  LIST_SUCCESS,
  LIST_FAILED,
  server,
} from "./../constants/index";

export const setStateListToFetching = () => ({
  type: LIST_FETCHING,
});

export const setStateListToSuccess = (payload) => ({
  type: LIST_SUCCESS,
  payload, //payload:payload
});

export const setStateListToFailed = () => ({
  type: LIST_FAILED,
});

export const getTravelling = () => {
  return async (dispatch) => {
    try {
      dispatch(setStateListToFetching());
      const result = await httpClient.get(server.TRAVELLINGLIST_URL);
      dispatch(setStateListToSuccess(result.data));
    } catch (e) {
      dispatch(setStateListToFailed());
    }
  };
};
