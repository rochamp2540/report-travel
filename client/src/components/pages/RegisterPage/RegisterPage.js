import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

import { useDispatch, useSelector } from "react-redux";
import * as registerActions from "../../../actions/register.action";

const styles = {
  background: {
    // backgroundColor: "#212529",
  },
  row: {
    marginTop: "7rem",
    marginRight: "0px",
  },
  txt1: {
    fontFamily: "Roboto",
    fontSize: "2.2rem",
    color: "#000",
    marginBottom: "1rem",
    fontWeight: "700",
    textAlign: "center",
  },
  txt2: {
    fontFamily: "Roboto",
    fontSize: "1rem",
    color: "#000",
  },
};

// object schema สำหรับทำ validation
const RegisterSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("This field is required."),
  password: Yup.string()
    .min(3, "Please Enter less then 3 letters")
    .required("This field is required."),
  confirmPassword: Yup.string()
    .min(3, "Please Enter less then 3 letters")
    .required("This field is required.")
    //check is password match ?
    .test("passwords-match", "Password not match.", function (value) {
      return this.parent.password === value;
    }),
});

const RegisterPage = (props) => {
  const dispatch = useDispatch();
  const { registerReducer } = useSelector((registerReducer) => registerReducer);
  return (
    <div style={styles.background}>
      <div className="row justify-content-center" style={styles.row}>
        <div className="col-md-3">
          <div className="row justify-content-center">
            <div className="col-md-12" style={styles.txt1}>
              REGISTER
            </div>
            <div className="col-md-12">
              <Formik
                initialValues={{
                  //กำหนด initialValues
                  username: "",
                  password: "",
                  confirmPassword: "",
                }}
                validationSchema={RegisterSchema} //กำหนด validationSchema
                onSubmit={(values) => {
                  dispatch(registerActions.register(values, props.history));
                }}
              >
                {(
                  { errors, touched } //ตรวจสอบว่ามีการ touch หรือ error หรือไม่
                ) => (
                  <Form>
                    {/* ช่องสำหรับกรอก name */}
                    <div className="form-group">
                      <label htmlFor="name" style={styles.txt2}>
                        Username
                      </label>
                      <Field
                        name="username"
                        type="text"
                        //เงื่อนไขในการแสดงผล css
                        className={`form-control ${
                          touched.username
                            ? errors.username
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="name"
                        placeholder="Enter username"
                      />
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="username"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก password */}
                    <div className="form-group">
                      <label htmlFor="password" style={styles.txt2}>
                        Password
                      </label>
                      <Field
                        name="password"
                        type="password"
                        // เงื่อนไข css
                        className={`form-control ${
                          touched.password
                            ? errors.password
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="password"
                        placeholder="Password"
                      />
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="password"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก Confirm Password */}
                    <div className="form-group">
                      <label htmlFor="confirmPassword" style={styles.txt2}>
                        Confirm Password
                      </label>
                      <Field
                        name="confirmPassword"
                        type="password"
                        // //เงื่อนไขแสดง css
                        className={`form-control ${
                          touched.confirmPassword
                            ? errors.confirmPassword
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="confirmPassword"
                        placeholder="Enter Confirm Password"
                      />
                      {/* // แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="confirmPassword"
                        className="invalid-feedback"
                      />
                    </div>
                    <button
                      type="submit"
                      className="btn btn-success"
                      style={{ width: "47%", margin: "0 7px 0 0" }}
                    >
                      SUBMIT
                    </button>
                    <button
                      type="button"
                      className="btn btn-outline-danger"
                      style={{ width: "47%", margin: "0 0 0 7px" }}
                      onClick={() => {
                        props.history.push("/");
                      }}
                    >
                      Cancel
                    </button>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;
