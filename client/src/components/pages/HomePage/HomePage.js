import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

const useStyles = makeStyles({
  root: {
    minWidth: 100,
  },
  root2: {
    minWidth: 100,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  center: {
    textAlign: "center",
  },
  row: {
    marginTop: "3rem",
  },
  bgCon: {
    backgroundColor: "#ED1C21",
  },
  bgRec: {
    backgroundColor: "#6BC71F",
  },
  bgHos: {
    backgroundColor: "#046391",
  },
  bgDead: {
    backgroundColor: "#797979",
  },
  colorfont: {
    color: "#FFF",
  },
});

const HomePage = () => {
  const [data, setData] = useState({});
  const [url, setUrl] = useState("https://covid19.th-stat.com/api/open/today");
  const classes = useStyles();

  useEffect(() => {
    const fetchData = async () => {
      var instance = axios.create(/*{
        transformRequest: (data, headers) => {
          delete headers.common["x-access-token"];
        },
      }*/);
      const result = await instance.get(url);
      setData(result.data);
      console.log(data);
    };
    fetchData();
  }, []);

  return (
    <div className={classes.row}>
      <Container maxWidth="md">
        <h3
          component="h1"
          color="textSecondary"
          style={{ color: "#000" }}
        >
          Coronavirus Status
        </h3>
      </Container>

      <Container maxWidth="md">
        <>
          <Card
            className={[classes.root, classes.pos, classes.bgCon].join(" ")}
          >
            <CardContent>
              <Typography
                component="p"
                color="textSecondary"
                className={classes.center}
                style={{ color: "#FFF" }}
              >
                Confirmed
              </Typography>
              <Typography
                color=""
                variant="h5"
                className={classes.center}
                style={{ color: "#FFF" }}
              >
                {data.Confirmed}
              </Typography>
              <Typography
                component="p"
                color="textSecondary"
                className={classes.center}
                style={{ color: "#FFF" }}
              >
                [+{data.NewConfirmed}]
              </Typography>
            </CardContent>
          </Card>
        </>

        <>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={4}>
              <Card className={(classes.root2, classes.bgRec)}>
                <CardContent>
                  <Typography
                    component="p"
                    color="textSecondary"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    Recovered
                  </Typography>
                  <Typography
                    color=""
                    variant="h5"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    {data.Recovered}
                  </Typography>
                  <Typography
                    component="p"
                    color="textSecondary"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    [+{data.NewRecovered}]
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Card className={(classes.root2, classes.bgHos)}>
                <CardContent>
                  <Typography
                    component="p"
                    color="textSecondary"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    Hospitalized
                  </Typography>
                  <Typography
                    color=""
                    variant="h5"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    {data.Hospitalized}
                  </Typography>
                  <Typography
                    component="p"
                    color="textSecondary"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    [+{data.NewHospitalized}]
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Card className={(classes.root2, classes.bgDead)}>
                <CardContent>
                  <Typography
                    component="p"
                    color="textSecondary"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    Deaths
                  </Typography>
                  <Typography
                    color=""
                    variant="h5"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    {data.Deaths}
                  </Typography>
                  <Typography
                    component="p"
                    color="textSecondary"
                    className={classes.center}
                    style={{ color: "#FFF" }}
                  >
                    [+{data.NewDeaths}]
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </>
      </Container>
    </div>
  );
};
export default HomePage;
