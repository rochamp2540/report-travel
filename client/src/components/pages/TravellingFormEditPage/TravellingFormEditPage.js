import React, { useEffect, useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Moment from "moment";

import { useDispatch, useSelector } from "react-redux";
import * as formActions from "../../../actions/travellingform.action";

const styles = {
  background: {
    // backgroundColor: "#212529",
    // backgroundColor : "#EAEAE1"
  },
  row: {
    marginTop: "1rem",
    marginRight: "0px",
  },
  txt1: {
    fontFamily: "Roboto",
    fontSize: "2.2rem",
    color: "#000",
    marginBottom: "1rem",
    fontWeight: "700",
    textAlign: "center",
  },
  txt2: {
    fontFamily: "Roboto",
    fontSize: "1rem",
    color: "#000",
  },
};

// object schema สำหรับทำ validation
const TravellingFormSchema = Yup.object().shape({
  province_id: Yup.string().required("This field is required."),
  // date_start: Yup.string().required("This field is required."),
  // date_end: Yup.string().required("This field is required."),
  date_start: Yup.date(),
  date_end: Yup.date().min(
    Yup.ref("date_start"),
    "end date can't be before start date"
  ),
  information: Yup.string()
    .min(3, "Please Enter less then 3 letters")
    .required("This field is required."),
  habitation: Yup.string()
    .min(3, "Please Enter less then 3 letters")
    .required("This field is required."),
  transport_id: Yup.string().required("This field is required."),
});

const TravellingFormEditPage = (props) => {
  const dispatch = useDispatch();
  const { travellingFormReducer } = useSelector(
    (travellingFormReducer) => travellingFormReducer
  );
  const user = localStorage.getItem("token");
  const [isAdmin, setAdmin] = useState(false);
  const checkUser = () => {
    if (user === "admin1") {
      setAdmin(true);
    } else {
      setAdmin(false);
    }
  };
  const checkApprove = () => {
    if (travellingFormReducer.result3) {
      if (travellingFormReducer.result3.acknowledge_by) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };
  useEffect(() => {
    let id = props.match.params.id;
    dispatch(formActions.getProvince());
    dispatch(formActions.getTransport());
    dispatch(formActions.getFormById(id));
    checkUser();
  }, []);

  const checkButton = () => {
    if (isAdmin) {
      return (
        <button
          type="submit"
          className="btn btn-success"
          style={{ width: "100%" }}
        >
          Approve
        </button>
      );
    } else {
      return (
        <>
          <button
            type="submit"
            className="btn btn-success"
            style={{ width: "47%", marginRight: "10px" }}
            hidden={checkApprove()}
          >
            Submit
          </button>
          <button
            type="button"
            className="btn btn-outline-danger"
            style={{ width: "47%" }}
            onClick={() => {
              props.history.goBack();
            }}
          >
            Cancel
          </button>
        </>
      );
    }
  };
  return (
    <div style={styles.background}>
      <div className="row justify-content-center" style={styles.row}>
        <div className="col-md-3">
          <div className="row justify-content-center">
            <div className="col-md-12" style={styles.txt1}>
              TravellingForm
            </div>
            <div className="col-md-12">
              <Formik
                enableReinitialize
                //กำหนด initialValues
                initialValues={
                  travellingFormReducer.result3
                    ? {
                        province_id: travellingFormReducer.result3.province_id,
                        date_start: Moment(
                          travellingFormReducer.result3.date_start
                        ).format("YYYY-MM-DD"),
                        date_end: Moment(
                          travellingFormReducer.result3.date_end
                        ).format("YYYY-MM-DD"),
                        information: travellingFormReducer.result3.information,
                        habitation: travellingFormReducer.result3.habitation,
                        transport_id:
                          travellingFormReducer.result3.transport_id,
                        created_by: travellingFormReducer.result3.created_by,
                        travelling_id:
                          travellingFormReducer.result3.travelling_id,
                        acknowledge_by: isAdmin ? user : "",
                      }
                    : {
                        province_id: "",
                        date_start: "",
                        date_end: "",
                        information: "",
                        habitation: "",
                        transport_id: "",
                        created_by: "",
                        travelling_id: "",
                      }
                }
                validationSchema={TravellingFormSchema} //กำหนด validationSchema
                onSubmit={(values) => {
                  dispatch(formActions.formUpdate(values, props.history));
                }}
              >
                {(
                  { errors, touched } //ตรวจสอบว่ามีการ touch หรือ error หรือไม่
                ) => (
                  <Form>
                    {/* ช่องสำหรับกรอก province_id */}
                    <div className="form-group">
                      <label htmlFor="province_id" style={styles.txt2}>
                        Province
                      </label>
                      <Field
                        name="province_id"
                        as="select"
                        //เงื่อนไขในการแสดงผล css
                        className={`form-control ${
                          touched.province_id
                            ? errors.province_id
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="province_id"
                        placeholder="Enter province_id"
                        disabled={isAdmin || checkApprove()}
                      >
                        <option value="" disabled selected>
                          Select your option
                        </option>
                        {/* map drop down */}
                        {travellingFormReducer.result
                          ? travellingFormReducer.result.map((result) => (
                              <option value={result.ProvinceID}>
                                {result.ProvinceEng}
                              </option>
                            ))
                          : []}
                      </Field>
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="province_id"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก date-start */}
                    <div className="form-group">
                      <label htmlFor="date_start" style={styles.txt2}>
                        Date_Start
                      </label>
                      <Field
                        name="date_start"
                        type="date"
                        // เงื่อนไข css
                        className={`form-control ${
                          touched.date_start
                            ? errors.date_start
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="date_start"
                        placeholder="date_start"
                        disabled={isAdmin || checkApprove()}
                      />
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="date_start"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก date-stop */}
                    <div className="form-group">
                      <label htmlFor="date_end" style={styles.txt2}>
                        Date_End
                      </label>
                      <Field
                        name="date_end"
                        type="date"
                        // เงื่อนไข css
                        className={`form-control ${
                          touched.date_end
                            ? errors.date_end
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="date_end"
                        placeholder="date_end"
                        disabled={isAdmin || checkApprove()}
                      />
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="date_end"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก information */}
                    <div className="form-group">
                      <label htmlFor="information" style={styles.txt2}>
                        Information
                      </label>
                      <Field
                        name="information"
                        type="text"
                        // เงื่อนไข css
                        className={`form-control ${
                          touched.information
                            ? errors.information
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="information"
                        placeholder="information"
                        disabled={isAdmin || checkApprove()}
                      />
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="information"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก habitation */}
                    <div className="form-group">
                      <label htmlFor="habitation" style={styles.txt2}>
                        Habitation
                      </label>
                      <Field
                        name="habitation"
                        type="text"
                        // เงื่อนไข css
                        className={`form-control ${
                          touched.habitation
                            ? errors.habitation
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="habitation"
                        placeholder="habitation"
                        disabled={isAdmin || checkApprove()}
                      />
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="habitation"
                        className="invalid-feedback"
                      />
                    </div>
                    {/* ช่องสำหรับกรอก transport_id */}
                    <div className="form-group">
                      <label htmlFor="transport_id" style={styles.txt2}>
                        Transport
                      </label>
                      <Field
                        name="transport_id"
                        as="select"
                        // เงื่อนไข css
                        className={`form-control ${
                          touched.transport_id
                            ? errors.transport_id
                              ? "is-invalid"
                              : "is-valid"
                            : ""
                        }`}
                        id="transport_id"
                        placeholder="transport_id"
                        disabled={isAdmin || checkApprove()}
                      >
                        <option value="" disabled selected>
                          Select your option
                        </option>
                        {travellingFormReducer.result2
                          ? travellingFormReducer.result2.map((result2) => (
                              <option value={result2.transport_id}>
                                {result2.transport}
                              </option>
                            ))
                          : []}
                      </Field>
                      {/* แสดง Error Message */}
                      <ErrorMessage
                        component="div"
                        name="transport_id"
                        className="invalid-feedback"
                      />
                    </div>
                    {checkButton()}
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TravellingFormEditPage;
