import React from "react";
import { shallow } from "enzyme";
import TravellingFormEditPage from "./TravellingFormEditPage";

describe("TravellingFormEditPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TravellingFormEditPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
