import React from "react";
import { shallow } from "enzyme";
import TravellingFormPage from "./TravellingFormPage";

describe("TravellingFormPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TravellingFormPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
