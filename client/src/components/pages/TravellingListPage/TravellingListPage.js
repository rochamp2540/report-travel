import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import MaterialTable from "material-table";
import { Typography, Grid } from "@material-ui/core";
import Moment from "react-moment";
import * as TrvellingListAction from "./../../../actions/travellinglist.action";

const TravellingListPage = (props) => {
  const dispatch = useDispatch();
  const { travellinglistReducer } = useSelector(
    (travellinglistReducer) => travellinglistReducer
  );

  useEffect(() => {
    dispatch(TrvellingListAction.getTravelling());
  }, []);

  const columns = [
    {
      title: "Province",
      field: "province_id",
      render: (rowData) => (
        <Typography variant="body1">{rowData.province_id}</Typography>
      ),
    },
    {
      title: "Created By",
      field: "created_by",
      render: (rowData) => (
        <a href={"/form/edit/" + rowData.travelling_id}>
          <Typography variant="body1">{rowData.created_by}</Typography>
        </a>
      ),
    },
    {
      title: "Information",
      field: "information",
      render: (rowData) => (
        <Typography variant="body1">{rowData.information}</Typography>
      ),
    },
    {
      title: "Date Start",
      field: "date_start",
      render: (rowData) => (
        <Typography variant="body1">
          <Moment format="DD/MM/YYYY">{rowData.date_start}</Moment>
        </Typography>
      ),
    },
    {
      title: "Date End",
      field: "date_end",
      render: (rowData) => (
        <Typography variant="body1">
          <Moment format="DD/MM/YYYY">{rowData.date_end}</Moment>
        </Typography>
      ),
    },
    {
      title: "Created Date",
      field: "created_date",
      render: (rowData) => (
        <Typography variant="body1">
          <Moment format="DD/MM/YYYY">{rowData.created_date}</Moment>
        </Typography>
      ),
    },
    {
      title: "Approve By",
      field: "acknowledge_by",
      render: (rowData) => (
        <Typography variant="body1" style={{ color: "red" }}>
          {rowData.acknowledge_by}
        </Typography>
      ),
    },
  ];
  return (
    <div style={{ maxWidth: "100%" }}>
      <MaterialTable
        columns={columns}
        data={travellinglistReducer.result ? travellinglistReducer.result : []}
        title="Demo Title"
      />
    </div>
  );
};

export default TravellingListPage;
