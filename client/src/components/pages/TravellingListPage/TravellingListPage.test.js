import React from "react";
import { shallow } from "enzyme";
import TravellingListPage from "./TravellingListPagePage";

describe("TravellingListPage", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TravellingListPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
