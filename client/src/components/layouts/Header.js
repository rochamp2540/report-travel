import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import * as loginActions from "./../../actions/login.action";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 5,
  },
}));

const Header = (props) => {
  // console.log(props)
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();
  const { loginReducer } = useSelector((state) => state);

  useEffect(() => console.log(loginReducer), [loginReducer]);

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {/* <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton> */}
          <Typography variant="h6" className={classes.title}>
            Report-Travial
            <Button
              // variant="text"
              color="inherit"
              // component={NavLink}
              // to="/"
              style={{ margin: "10px", padding: "8px" }}
              onClick={() => {
                history.push("/");
              }}
            >
              Home
            </Button>
            <Button
              // variant="text"
              color="inherit"
              style={{ margin: "10px", padding: "8px" }}
              onClick={() => {
                history.push("/form");
              }}
            >
              Form
            </Button>
            <Button
              // variant="text"
              color="inherit"
              style={{ margin: "10px", padding: "8px" }}
              onClick={() => {
                history.push("/list");
              }}
            >
              TravellingList
            </Button>
          </Typography>
          {loginActions.isLoggedIn() ? (
            <Button
              color="inherit"
              onClick={() => {
                dispatch(loginActions.logout());
                history.push("/");
              }}
            >
              Logout
            </Button>
          ) : (
            <Button
              color="inherit"
              onClick={() => {
                history.push("/login");
              }}
            >
              Login
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
