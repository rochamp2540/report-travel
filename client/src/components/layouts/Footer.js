import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "20px",
  },
}));

const Header = (props) => {
  const classes = useStyles();

  return <div className={classes.root}></div>;
};

export default Header;
