import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import "./App.css";
import RegisterPage from "./components/pages/RegisterPage";
import LoginPage from "./components/pages/LoginPage";
import HomePage from "./components/pages/HomePage";
import Header from "./components/layouts/Header";
import Footer from "./components/layouts/Footer";
import TravellingFormPage from "./components/pages/TravellingFormPage";
import TravellingFormEditPage from "./components/pages/TravellingFormEditPage";
import TravellingListPage from "./components/pages/TravellingListPage";
import * as loginActions from "./actions/login.action";

// Protected Route
const SecuredRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      // ternary condition
      loginActions.isLoggedIn() ? (
        <Component {...props} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

const LoginRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      // ternary condition
      loginActions.isLoggedIn() ? <Redirect to="/" /> : <LoginPage {...props} />
    }
  />
);

function App() {
  return (
    <Router>
      <Switch>
        <div>
          <Header />
          {/* <Route path="/" component={() => <Redirect to="/login" />} /> */}
          <Route path="/" exact={true} component={HomePage} />
          <Route path="/register" component={RegisterPage} />
          <LoginRoute path="/login" component={LoginPage} />
          <SecuredRoute
            path="/form"
            exact={true}
            component={TravellingFormPage}
          />
          <SecuredRoute
            path="/form/edit/:id"
            component={TravellingFormEditPage}
          />
          <SecuredRoute path="/list" component={TravellingListPage} />
          <Footer />
        </div>
      </Switch>
    </Router>
  );
}

export default App;
